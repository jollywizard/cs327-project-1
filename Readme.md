## Project Structure

The application is an HTML/CSS/JS application.  All resources are embedded in the html file.

Test files are provided in `datafiles/`.


## Compilation Instructions

No compilation is necessary.  Simply open the file in a compliant browser.


## Browser Compliance

The browser must support HTML5 local file storage API.  This is technically still a working draft, but widely supported.

Tested in:
* Firefox 44
* Chrome 48
* Microsoft Edge 25


## Usage

There are three layouts based on the width of the screen.  

* The application controls will appear along the left side of the screen.
* Application output will appear to the right of or below the controls depending on your screen size.

Use the graphical controls to set the cypher keys, crypto mode, and target file.

Diagnostic information about parameters appears at the top of the output area (outlined in red).

Output / diagnostics are updated each time controls are triggered.

Once all parameters are set successfully, the diagnostic information will disappear and the output will take it's place.


## Fixtures

Tests data, verified using third party applications, is provided in `datafiles/`.

File names for encoded files are of the form: `xxxxx-a-b.txt` and correspond to encoding file `xxxxx.txt`.